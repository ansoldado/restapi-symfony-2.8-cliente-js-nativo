console.log('Cargando app.js');

// Ajax conection object
var xmlhttp = new XMLHttpRequest();

// Jobs container
var listaContenedor = document.getElementById('lista-tareas');

// New job input
var newJobInput = document.getElementById('job-input');

// Messages container
var messagesContainer = document.getElementById('messages');
// Messages header
var messagesHeader = document.getElementById('messageHeader');
// Messages text
var message = document.getElementById('message');

function showMessage(messageParameter, title ,error) {
    console.log("Mostrando mensage.", message, title, error);
    let erroClass = "bg-success";
    if(error) {
        erroClass = "bg-danger";
        newJobInput.setAttribute("isvalid", "false");        
    } else {
        newJobInput.setAttribute("isvalid", "true");    
    }

    messagesContainer.className = "animated messages card " + erroClass + " hide";
    message.innerHTML = messageParameter;
    messagesHeader.innerHTML = title;
    setTimeout(( function() {
        messagesContainer.className = "animated messages card " + erroClass + " show";        
    }), 100);
    setTimeout( function() {
        messagesContainer.className = "animated messages card " + erroClass + "  hide";
        setTimeout( function() {
            messagesContainer.className = "hideTotal";
        }, 700 );
    }, 3100 );
}

function newDomJob(job) {
    // Creo el nuevo elemento
    let nuevaTarea = document.createElement('li');
    
    // Recupero el numero de tareas hasta la fecha
    let numeroTarea = listaContenedor.children.length;
    
    // Añado el contenido a la nueva tarea
    nuevaTarea.innerHTML = job.description;
    nuevaTarea.className = "animated list-group-item expand";

    // Añado la nueva tarea a la lista
    listaContenedor.appendChild(nuevaTarea);

    // Animo la tarea
    setTimeout(function() {
        nuevaTarea.className = "animated list-group-item";        
    }, 700);
}

function recuperarJobs() {
    console.log('Recuperando jobs...');
    let jobs = window.localStorage.getItem('jobs');
    let errors = new Array();
    if(!jobs) {
        console.log('Recuperando del servidor.');        
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                listaContenedor.innerHTML = "";                
                jobs = JSON.parse(this.responseText).jobs;
                let jobIndex = 0;
                var interval = setInterval(function(){
                    newDomJob(jobs[jobIndex]);
                    jobIndex++;                
                    if(jobIndex == jobs.length) {
                        clearInterval(interval);
                    }
                }, 700);
                window.localStorage.setItem("jobs", JSON.stringify(jobs));            
            }
            if (this.status == 204) {
                listaContenedor.innerHTML = "";                
                newDomJob({description: 'No jobs inserted.'});
            }
            if (this.status == 400) {
                errors = JSON.parse(this.responseText).errors;                                
                showMessage(errors[0], "Conection error.", true);
            }
        };
        xmlhttp.open("GET", "http://localhost:8000/jobs");
        xmlhttp.send();
    } else {
        console.log('Recuperando del webstorage.');                
        let jobsParsed = JSON.parse(jobs);
        let jobIndex = 0;
        var interval = setInterval(function(){
            newDomJob(jobsParsed[jobIndex]);
            jobIndex++;                
            if(jobIndex == jobsParsed.length) {
                clearInterval(interval);
            }
        }, 700);
    }
}

function postNewJob() {

    let newJob = newJobInput.value;
    let newJobJson = encodeURI("description="+newJob);

    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            let responseParsed = JSON.parse(this.responseText);
            let job = responseParsed.data;                  
            if(job) {
                showMessage("Job with id " + job.id + " inserted." , "Insert succesful.", false);
                let jobs = window.localStorage.getItem('jobs');
                let jobsParsed = JSON.parse(jobs);
                console.log(jobs);
                if(jobsParsed === null) {
                    listaContenedor.innerHTML = "";
                    let jobsArray = new Array();
                    jobsArray.push(job);
                    window.localStorage.setItem('jobs', JSON.stringify(jobsArray));                       
                } else {
                    jobsParsed.push(job);
                    window.localStorage.setItem('jobs', JSON.stringify(jobsParsed));   
                }
                newDomJob(job);                
            }
        }
        if (this.readyState == 4 && this.status == 400) {
            console.log(this.responseText);
            let responseParsed = JSON.parse(this.responseText);
            let errors = responseParsed.errors;                  
            showMessage(errors[0], "Validation error.", true);
        }
        if (this.readyState == 4 && this.status == 500) {
            console.log(this.responseText);
            let responseParsed = JSON.parse(this.responseText);
            let errors = responseParsed.errors;                  
            showMessage(errors[0], "Server error.", true);
        }
    };
    xmlhttp.open("POST", "http://localhost:8000/create-job", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(newJobJson);
    newJobInput.value = "";
    newJobInput.innerHTML = "";
}

recuperarJobs();