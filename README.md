# Descripcion:

## Servidor

Servidor basado en symfony 2.8 con 2 rutas disponibles

### Rutas
* /jobs: 
Devuelve la lista completa de jobs almacenada en una base de datos mysql mediate una petición GET.
* /create-job: 
Añade un nuevo job mediante una petición post, tiene las siguientes restricciones:
	+ Descripción no puede ser nula.
	+ Descripción debe tener entre 20 y 50
	+ Descripción no puede estar repetida.

## Cliente

El cliente es una aplicación web con javascript nativo, css y html5.
Hace uso de localstorage en el que almacena los jobs una vez recibidos del servidor.

# Instalación:

### Cliente:
El cliente no necesita instalación al tratarse de una aplicación web.

### Servidor:
* Antes de lanzar el servidor es necesario ejecutar los siguientes comandos:

	
		php app/console doctrine:database:create
	
		php app/console doctrine:schema:update --force
	
	Es necesario tener un servidor mysql arrancado con el usuario por defecto activo (root / root).
	
* Lanzar el servidor:

		php app/console server:run