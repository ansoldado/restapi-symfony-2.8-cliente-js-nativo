<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use \Datetime;

/**
 * @ORM\Entity
 * @ORM\Table(name="jobs")
 */
class Job
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(type="string", length=50, unique=true)
     * @Assert\NotBlank
     * @Assert\Length(
     *      min = 20,
     *      max = 50,
     * )
     */     
    private $description;
    
    /**
     * @ORM\Column(type="datetime")
     */     
    private $timestamp;

    public function __construct()
    {
        $this->timestamp = new DateTime(); 
    }

    public function getId() {
        return $this->id;
    }

    public function getDescription() {
        return $this->description;
    }

    public function getTimestamp() {
        return $this->timestamp;
    }

    public function setDescription($description) {
        $this->description = $description;
    }
}