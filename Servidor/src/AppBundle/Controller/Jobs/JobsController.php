<?php

namespace AppBundle\Controller\Jobs;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Job;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use \Doctrine\ORM\ORMException;

class JobsController extends Controller
{
    /**
     * @Route("/jobs", name="jobs")
     */
    public function indexAction(Request $request)
    {
        $errors = array();
        $jobsFormated = array();
        try{
            $em = $this->getDoctrine()->getManager();
            $jobs = $em->getRepository(Job::class)->findAll();
         }catch(\Exception $e){
            $e->getMessage();
            array_push($errors, "Dabatase exeption."); 
            return new JsonResponse(array('jobs' => $jobsFormated, 'errors' => $errors), 400, array('Access-Control-Allow-Origin' => '*', 'Content-Type' => 'application/json') );
        }        

        foreach ($jobs as $job) {
            $jobsFormated[] = array(
                'id' => $job->getId(),
                'description' => $job->getDescription(),
                'timestamp' => $job->getTimestamp(),
            );
        }

        if(count($jobs) > 0) {            
            return new JsonResponse(array('jobs' => $jobsFormated, 'errors' => $errors), 200, array('Access-Control-Allow-Origin' => '*', 'Content-Type' => 'application/json') );
        }

        return new JsonResponse(array('jobs' => $jobsFormated, 'errors' => $errors), 204, array('Access-Control-Allow-Origin' => '*', 'Content-Type' => 'application/json') );
    }
}
