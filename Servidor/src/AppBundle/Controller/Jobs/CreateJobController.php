<?php

namespace AppBundle\Controller\Jobs;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Job;
use AppBundle\Entity\JobType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use \Doctrine\ORM\ORMException;

class CreateJobController extends Controller
{
    /**
     * @Route("/create-job", name="create-job")
     */
    public function indexAction(Request $request)
    {   
        $job = new Job();
        $job->setDescription($request->request->get('description'));
        $form = $this->createForm(new JobType(), $job);
        $errors = array();
        $status = 200;
        $data =  $request->request->all();
        $form->submit($data);        
        //$form->handleRequest($request);
        if ($form->isValid()) {
            try{
                $em = $this->getDoctrine()->getManager();
                $em->persist($job);
                $em->flush();
             }catch(UniqueConstraintViolationException $e){
                array_push($errors, "Duplicated job description."); 
                $status = 400;               
             }catch(\Exception $e){
                $e->getMessage();
                array_push($errors, "Cannot conect with database."); 
                return new JsonResponse(array('errors' => $errors), 500, array('Access-Control-Allow-Origin' => '*', 'Content-Type' => 'application/json') );
            }             

             if($status == 200) {
                $response = new JsonResponse(array('code' => '200', 'errors' => $errors, 'data' => array(
                    'id' => $job->getId(),
                    'description' => $job->getDescription(),
                    'timestamp' => $job->getTimestamp(),
                )), 200);
             } else {
                $response = new JsonResponse(array('code' => '400', 'errors' => $errors, 'data' => array()), 400);
             }

            $response->headers->set('Access-Control-Allow-Origin', '*');
            $response->headers->set('Content-Type','application/json');

            return $response;

        }

        $validator = $this->get('validator');
        $errorsValidator = $validator->validate($job);
        
        foreach ($errorsValidator as $error) {
            array_push($errors, $error->getMessage());
        }


        $response = new JsonResponse(array('code' => '400', 'errors' => $errors, 'data' => array()), 400);
        $response->headers->set('Access-Control-Allow-Origin', '*');
        return $response;
    }
}
